from page_objects import PageObject, PageElement



class WelcomePage(PageObject):

    login_link = PageElement(link_text="Login")
    home_link = PageElement(link_text="Home")

    def check_page(self):
        return "Welcome" in self.w.title


    def click_home_link(self, url):
        self.home_link.click()
        return url in self.w.current_url()

    def click_login(self, loginpage):
        self.login_link.click()
        return loginpage.check_page()
